﻿using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;

public class PlayFabLoginWithCustomId : MonoBehaviour
{
    void Start()
    {
        var request = new LoginWithCustomIDRequest {CustomId = "MyComputer", CreateAccount = true};
        PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailed);
    }

    private void OnLoginFailed(PlayFabError error)
    {
        Debug.Log("Login Error : " + error.GenerateErrorReport());
    }

    private void OnLoginSuccess(LoginResult result)
    {
        Debug.Log("Login Success");
    }
}
